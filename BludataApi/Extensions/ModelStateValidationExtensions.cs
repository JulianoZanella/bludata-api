﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BludataApi.Extensions
{
    public static class ModelStateValidationExtensions
    {
        public static IServiceCollection ConfigureModelStateValidation(this IServiceCollection services)
        {
            return services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var problemDetails = new ValidationProblemDetails(context.ModelState)
                    {
                        Instance = context.HttpContext.Request.Path,
                        Status = StatusCodes.Status422UnprocessableEntity,
                        Detail = "Veja os erros para mais detalhes",
                        Title = "Erro na validação"
                    };

                    return new BadRequestObjectResult(new CustomProblemDetails(problemDetails))
                    {
                        ContentTypes = { "application/problem+json" }
                    };
                };
            });
        }
    }

    public class CustomProblemDetails : ValidationProblemDetails
    {
        public List<ErrorField> ErrorsFields { get; set; } = new List<ErrorField>();
        public CustomProblemDetails(ModelStateDictionary modelState) : base(modelState)
        {
        }

        public CustomProblemDetails(ValidationProblemDetails problemDetails)
        {
            Title = problemDetails.Title;
            Status = problemDetails.Status;
            Instance = problemDetails.Instance;
            Detail = problemDetails.Detail;
            Type = problemDetails.Type;

            foreach (var error in problemDetails.Errors)
            {
                Errors.Add(error);
                var eField = new ErrorField()
                {
                    Field = error.Key
                };
                eField.Errors = error.Value.ToList();
                ErrorsFields.Add(eField);
            }
        }

    }

    public class ErrorField
    {
        public String Field { get; set; }
        public List<String> Errors { get; set; } = new List<String>();
    }
}
