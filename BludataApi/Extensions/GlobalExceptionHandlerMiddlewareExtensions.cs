﻿using BludataApi.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;

namespace BludataApi.Extensions
{
    public static class GlobalExceptionHandlerMiddlewareExtensions
    {
        public static void UseGlobalExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(builder =>
            {
                builder.Run(async context =>
                {
                    var exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();

                    if (exceptionHandlerFeature != null)
                    {
                        var ex = exceptionHandlerFeature.Error;

                        var problemDetails = ErrorFilterUtil.Intercept(context, ex);

                        await context.Response.WriteAsync(JsonConvert.SerializeObject(problemDetails));
                    }
                });
            });
        }

    }
}
