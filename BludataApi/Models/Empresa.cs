﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BludataApi.Models
{
    public class Empresa
    {
        public int Id { get; set; }

        [Required]
        [StringLength(2, ErrorMessage ="Apenas UF")]
        public string UF { get; set; }

        [Required]
        public string NomeFantasia { get; set; }

        [Required]
        public string CNPJ { get; set; }
        public List<Fornecedor> Fornecedores { get; set; } = new List<Fornecedor>();

        public Empresa()
        {                
        }

        public Empresa(string nomeFantasia, string cNPJ, string uF)
        {           
            NomeFantasia = nomeFantasia;
            CNPJ = cNPJ;
            UF = uF;
        }

        public void AddFornecedor(Fornecedor fornecedor)
        {
            Fornecedores.Add(fornecedor);
        }

        public void RemoveFornecedor(Fornecedor fornecedor)
        {
            Fornecedores.Remove(fornecedor);
        }

        public bool IsParanaense()
        {
            return UF.Equals("PR");
        }
    }
}
