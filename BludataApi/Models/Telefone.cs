﻿
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace BludataApi.Models
{
    public class Telefone
    {
        public int Id { get; set; }

        [Required]
        public string Numero { get; set; }

        [JsonIgnore]
        public Fornecedor Fornecedor { get; set; }

        [Required]
        public int FornecedorId { get; set; }

        public Telefone()
        {
        }

        public Telefone(string numero, Fornecedor fornecedor)
        {
            Numero = numero;
            Fornecedor = fornecedor;
        }
    }
}
