﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BludataApi.Models
{
    public class Fornecedor
    {
        public int Id { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public string CPFouCNPJ { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DataCadastro { get; set; } = DateTime.UtcNow;
        public string RG { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DataNascimento { get; set; }

        public List<Telefone> Telefones { get; set; } = new List<Telefone>();

        [JsonIgnore]
        public Empresa Empresa { get; set; }

        [Required]
        public int EmpresaId { get; set; }

        public Fornecedor()
        {
        }

        public Fornecedor(string nome, string cPFouCNPJ, DateTime dataCadastro, Empresa empresa)
        {
            Nome = nome;
            CPFouCNPJ = cPFouCNPJ;
            DataCadastro = dataCadastro;
            Empresa = empresa;
        }

        public Fornecedor(string nome, string cPFouCNPJ, Empresa empresa)
        {
            Nome = nome;
            CPFouCNPJ = cPFouCNPJ;
            Empresa = empresa;
        }

        public Fornecedor(string nome, string cPFouCNPJ, string rG, DateTime dataNascimento, Empresa empresa)
        {
            Nome = nome;
            CPFouCNPJ = cPFouCNPJ;
            RG = rG;
            DataNascimento = dataNascimento;
            Empresa = empresa;
        }

        public void AddTelefone(Telefone telefone)
        {
            Telefones.Add(telefone);
        }

        public void RemoveTelefone(Telefone telefone)
        {
            Telefones.Remove(telefone);
        }

        public bool IsMaiorDeIdade()
        {
            if (!DataNascimento.HasValue) return false;
            var now = DateTime.Now;
            int idade = now.Year - DataNascimento.Value.Year;
            if (DataNascimento.Value.Date > now.AddYears(-idade)) idade--;
            return idade >= 18;
        }

        public bool IsPessoaFisica()
        {
            return RG != null || DataNascimento.HasValue;
        }
    }
}
