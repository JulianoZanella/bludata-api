﻿using BludataApi.Models;
using System;
using System.Linq;

namespace BludataApi.Data
{
    public class SeedingService
    {
        private readonly DBContext _context;

        public SeedingService(DBContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            if (_context.Empresa.Any())
            {
                return;
            }

            Empresa e1 = new Empresa("Teste 1", "000.123.156/0001", "PR");
            Empresa e2 = new Empresa("Teste 2", "000.123.156/0002", "SC");
            Empresa e3 = new Empresa("Teste 3", "000.123.156/0003", "SC");
            Empresa e4 = new Empresa("Teste 4", "000.123.156/0004", "PR");

            Fornecedor f1 = new Fornecedor("Baleia", "123.46.789/02", e1);
            Fornecedor f2 = new Fornecedor("Geléia", "123.46.789/02", "1.877.647", DateTime.Parse("03/01/1985"), e1);

            Telefone t1 = new Telefone("3333-1502", f2);
            Telefone t2 = new Telefone("3333-5689", f2);
            f2.AddTelefone(t1);
            f2.AddTelefone(t2);

            e1.AddFornecedor(f1);
            e1.AddFornecedor(f2);

            _context.Empresa.AddRange(e1, e2, e3, e4);
            _context.Fornecedor.AddRange(f1, f2);
            _context.Telefone.AddRange(t1, t2);

            _context.SaveChanges();
        }
    }
}
