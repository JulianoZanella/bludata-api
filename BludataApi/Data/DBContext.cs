﻿using Microsoft.EntityFrameworkCore;
using BludataApi.Models;
using System.Linq;

namespace BludataApi.Data
{
    public class DBContext : DbContext
    {
        public DbSet<Empresa> Empresa { get; set; }
        public DbSet<Fornecedor> Fornecedor { get; set; }
        public DbSet<Telefone> Telefone { get; set; }

        public DBContext (DbContextOptions<DBContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Definindo On Delete = No Action
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            modelBuilder.Entity<Telefone>().HasOne(x => x.Fornecedor).WithMany(x => x.Telefones).OnDelete(DeleteBehavior.Cascade);
            base.OnModelCreating(modelBuilder);
        }
    }
}
