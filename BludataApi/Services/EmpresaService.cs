﻿using BludataApi.Data;
using BludataApi.Models;
using BludataApi.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BludataApi.Services
{
    public class EmpresaService
    {
        private readonly DBContext _context;

        public EmpresaService(DBContext context)
        {
            _context = context;
        }

        public async Task<List<Empresa>> FindAllAsync()
        {
            return await _context.Empresa.ToListAsync();
        }

        public async Task InsertAsync(Empresa empresa)
        {
            _context.Add(empresa);
            await _context.SaveChangesAsync();
        }

        public async Task<Empresa> FindByIdAsync(int id)
        {
            return await _context.Empresa.Include(x => x.Fornecedores).FirstOrDefaultAsync(x => x.Id == id);           
        }

        public async Task<Empresa> DeleteAsync(int id)
        {
            try
            {
                var empresa = await _context.Empresa.FindAsync(id);
                if (empresa != null)
                {
                    _context.Empresa.Remove(empresa);
                    await _context.SaveChangesAsync();
                    return empresa;
                }
                else
                {
                    throw new NotFoundException($"Empresa com id={id} não encontrada");
                }
            }
            catch (DbUpdateException)
            {
                throw new IntegrityException("Há fornecedores relacionados a esta Empresa");
            }
        }

        public async Task UpdateAsync(Empresa empresa)
        {
            var hasAny = await _context.Empresa.AnyAsync(x => x.Id == empresa.Id);
            if (!hasAny)
            {
                throw new NotFoundException();
            }
            try
            {
                _context.Update(empresa);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw new DBConcurrencyException(e.Message);
            }
        }
    }
}
