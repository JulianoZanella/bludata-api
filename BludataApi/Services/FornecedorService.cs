﻿using BludataApi.Data;
using BludataApi.Models;
using BludataApi.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace BludataApi.Services
{
    public class FornecedorService
    {
        private readonly DBContext _context;

        public FornecedorService(DBContext context)
        {
            _context = context;
        }

        public async Task<List<Fornecedor>> FindAllAsync(
            string nome, string cpfOuCnpj, DateTime? cadatroMin, DateTime? cadastroMax)
        {
            var result = from obj in _context.Fornecedor select obj;
            if (nome != null)
            {
                result = result.Where(x => x.Nome.Contains(nome));
            }
            if (cpfOuCnpj != null)
            {
                result = result.Where(x => x.CPFouCNPJ.Contains(cpfOuCnpj));
            }
            if (cadatroMin.HasValue)
            {
                result = result.Where(x => x.DataCadastro >= cadatroMin);
            }
            if (cadastroMax.HasValue)
            {
                result = result.Where(x => x.DataCadastro <= cadastroMax);
            }
            return await result.ToListAsync();
        }

        public async Task InsertAsync(Fornecedor fornecedor)
        {
            await VerifyPessoaFisicaAsync(fornecedor);
            _context.Add(fornecedor);
            await _context.SaveChangesAsync();
        }

        public async Task<Fornecedor> FindByIdAsync(int id)
        {
            return await _context.Fornecedor.Include(x => x.Telefones).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Fornecedor> DeleteAsync(int id)
        {
            try
            {
                var fornecedor = await _context.Fornecedor.FindAsync(id);
                if (fornecedor != null)
                {
                    _context.Fornecedor.Remove(fornecedor);
                    await _context.SaveChangesAsync();
                    return fornecedor;
                }
                else
                {
                    throw new NotFoundException($"Fornecedor com id={id} não encontrado");
                }
            }
            catch (DbUpdateException e)
            {
                throw new IntegrityException(e.Message);
            }
        }

        public async Task UpdateAsync(Fornecedor fornecedor)
        {
            var hasAny = await _context.Fornecedor.AnyAsync(x => x.Id == fornecedor.Id);
            if (!hasAny)
            {
                throw new NotFoundException();
            }

            await VerifyPessoaFisicaAsync(fornecedor);

            try
            {
                _context.Update(fornecedor);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw new DBConcurrencyException(e.Message);
            }
        }

        private async Task VerifyPessoaFisicaAsync(Fornecedor fornecedor)
        {
            if (fornecedor.IsPessoaFisica())
            {
                if (fornecedor.RG == null || !fornecedor.DataNascimento.HasValue)
                {
                    throw new PessoaFisicaException();
                }
                Empresa empresa = await _context.Empresa.AsNoTracking().FirstOrDefaultAsync(x => x.Id == fornecedor.EmpresaId);
                fornecedor.Empresa = empresa;
                if (empresa == null)
                {
                    throw new NotFoundException();
                }
                if (empresa.IsParanaense() && !fornecedor.IsMaiorDeIdade())
                {
                    throw new MenorParanaenseException();
                }
            }
        }
    }
}
