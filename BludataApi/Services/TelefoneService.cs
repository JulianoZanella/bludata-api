﻿using BludataApi.Data;
using BludataApi.Models;
using BludataApi.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BludataApi.Services
{
    public class TelefoneService
    {
        private readonly DBContext _context;

        public TelefoneService(DBContext context)
        {
            _context = context;
        }

        public async Task<List<Telefone>> FindAllAsync()
        {
            return await _context.Telefone.ToListAsync();
        }

        public async Task InsertAsync(Telefone telefone)
        {
            _context.Add(telefone);
            await _context.SaveChangesAsync();
        }

        public async Task<Telefone> FindByIdAsync(int id)
        {
            return await _context.Telefone.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Telefone>> FindByFornecedorIdAsync(string idString)
        {
            int id = 0;
            try
            {
                id = int.Parse(idString);
            }
            catch (Exception)
            {
                return null;
            }
            return await _context.Telefone.Where(x => x.FornecedorId == id).ToListAsync();
        }

        public async Task<Telefone> DeleteAsync(int id)
        {
            try
            {
                var telefone = await _context.Telefone.FindAsync(id);

                if (telefone == null) throw new NotFoundException($"Telefone com id={id} não encontrado");

                _context.Telefone.Remove(telefone);
                await _context.SaveChangesAsync();
                return telefone;

            }
            catch (DbUpdateException e)
            {
                throw new IntegrityException(e.Message);
            }
        }

        public async Task UpdateAsync(Telefone telefone)
        {
            var hasAny = await _context.Telefone.AnyAsync(x => x.Id == telefone.Id);
            if (!hasAny)
            {
                throw new NotFoundException();
            }
            try
            {
                _context.Update(telefone);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw new DBConcurrencyException(e.Message);
            }
        }
    }
}
