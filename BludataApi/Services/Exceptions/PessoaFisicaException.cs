﻿using System;

namespace BludataApi.Services.Exceptions
{
    public class PessoaFisicaException: ApplicationException
    {
        public PessoaFisicaException(): base("Para pessoa física é obrigatório: RG e Data de Nascimento")
        {
        }

        public PessoaFisicaException(string msg): base(msg)
        {
        }
    }
}
