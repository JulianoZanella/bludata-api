﻿using System;

namespace BludataApi.Services.Exceptions
{
    public class NotFoundException: ApplicationException
    {
        public NotFoundException() : base("Não encontrado")
        {
        }

        public NotFoundException(string msg): base(msg)
        {
        }
    }
}
