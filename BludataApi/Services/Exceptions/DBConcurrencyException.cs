﻿using System;

namespace BludataApi.Services.Exceptions
{
    public class DBConcurrencyException: ApplicationException
    {
        public DBConcurrencyException(): base("Erro de concorrência de dados")
        {
        }

        public DBConcurrencyException(string msg): base(msg)
        {
        }
    }
}
