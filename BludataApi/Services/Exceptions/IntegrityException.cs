﻿using System;

namespace BludataApi.Services.Exceptions
{
    public class IntegrityException: ApplicationException
    {
        public IntegrityException(): base("Não pode deletar campos com dependentes")
        {
        }

        public IntegrityException(string msg): base(msg)
        {
        }
    }
}
