﻿using System;

namespace BludataApi.Services.Exceptions
{
    public class MenorParanaenseException: PessoaFisicaException
    {
        public MenorParanaenseException(): base("O fornecedor pessoa física não pode ser menor de idade no Paraná")
        {
        }
    }
}
