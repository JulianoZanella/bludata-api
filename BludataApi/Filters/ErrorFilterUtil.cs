﻿using BludataApi.Services.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data.SqlClient;

namespace BludataApi.Filters
{
    public static class ErrorFilterUtil
    {
        public static ProblemDetails Intercept(HttpContext context, Exception ex)
        {
            var problemDetails = new ProblemDetails()
            {
                Instance = context.Request.HttpContext.Request.Path,
                Detail = ex.Message,
                Type = "application/problem+json"
            };

            if (ex is NullReferenceException || ex is NotFoundException)
            {
                problemDetails.Title = "Não encontrado";
                context.Response.StatusCode = StatusCodes.Status404NotFound;
            }
            else if (ex is IntegrityException)
            {
                problemDetails.Title = "Erro de integridade";
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
            }
            else if (ex is PessoaFisicaException)
            {
                problemDetails.Title = "Erro de falta de campos pessoa física";
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
            }
            else if (ex is DBConcurrencyException)
            {
                problemDetails.Title = "Erro no BD";
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
            }
            else if (ex is MenorParanaenseException)
            {
                problemDetails.Title = "Erro na data de nascimento";
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
            }
            else if (ex.InnerException is SqlException)
            {
                problemDetails.Title = "Erro no BD";
                var lenght = (ex.InnerException.Message.Length >= 63) ? 63 : ex.InnerException.Message.Length;
                problemDetails.Detail = ex.InnerException.Message.Substring(0, lenght);
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
            }
            else
            {
                problemDetails.Title = "Erro desconhecido";
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            }


            context.Response.ContentType = "application/problem+json";
            problemDetails.Status = context.Response.StatusCode;

            return problemDetails;

        }
    }
}
