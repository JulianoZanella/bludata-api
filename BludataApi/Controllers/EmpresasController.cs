﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BludataApi.Models;
using BludataApi.Services;

namespace BludataApi.Controllers
{
    [Route("api/empresas")]
    [ApiController]
    public class EmpresasController : ControllerBase
    {
        private readonly EmpresaService _service;

        public EmpresasController(EmpresaService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IEnumerable<Empresa>> GetEmpresas()
        {
            return await _service.FindAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetEmpresa([FromRoute] int id)
        {
            var empresa = await _service.FindByIdAsync(id);

            if (empresa == null) return NotFound();

            return Ok(empresa);
        }

        [HttpPost]
        public async Task<IActionResult> PostEmpresa([FromBody] Empresa empresa)
        {
            await _service.InsertAsync(empresa);

            return CreatedAtAction("GetEmpresa", new { id = empresa.Id }, empresa);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmpresa([FromRoute] int id)
        {
            var empresa = await _service.DeleteAsync(id);
            return Ok(empresa);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmpresa([FromRoute] int id, [FromBody] Empresa empresa)
        {
            if (id != empresa.Id)
            {
                return BadRequest();
            }
            await _service.UpdateAsync(empresa);

            return NoContent();
        }
    }
}