﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BludataApi.Models;
using BludataApi.Services;

namespace BludataApi.Controllers
{
    [Route("api/telefones")]
    [ApiController]
    public class TelefonesController : ControllerBase
    {
        private readonly TelefoneService _service;

        public TelefonesController(TelefoneService service)
        {
            _service = service;
        }

        [HttpGet]//api/telefones/?fornecedorId=1
        public async Task<IEnumerable<Telefone>> GetTelefones(string fornecedorId)
        {
            if (fornecedorId != null && fornecedorId.Length > 0)
            {
                return await _service.FindByFornecedorIdAsync(fornecedorId);
            }
            return await _service.FindAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTelefone([FromRoute] int id)
        {
            var telefone = await _service.FindByIdAsync(id);

            if (telefone == null) return NotFound();
            return Ok(telefone);
        }

        [HttpPost]
        public async Task<IActionResult> PostTelefone([FromBody] Telefone telefone)
        {
            await _service.InsertAsync(telefone);
            return CreatedAtAction("GetTelefone", new { id = telefone.Id }, telefone);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTelefone([FromRoute] int id)
        {
            var telefone = await _service.DeleteAsync(id);
            return Ok(telefone);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutTelefone([FromRoute] int id, [FromBody] Telefone telefone)
        {
            if (id != telefone.Id) return BadRequest();

            await _service.UpdateAsync(telefone);
            return NoContent();
        }
    }
}