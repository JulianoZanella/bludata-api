﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BludataApi.Models;
using BludataApi.Services;
using System;

namespace BludataApi.Controllers
{
    [Route("api/fornecedores")]
    [ApiController]
    public class FornecedoresController : ControllerBase
    {
        private readonly FornecedorService _service;

        public FornecedoresController(FornecedorService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IEnumerable<Fornecedor>> GetFornecedores(
            string nome, string cpfOuCnpj, DateTime? cadastroMin, DateTime? cadastroMax)
        {
            return await _service.FindAllAsync(nome, cpfOuCnpj, cadastroMin, cadastroMax);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetFornecedor([FromRoute] int id)
        {
            var fornecedor = await _service.FindByIdAsync(id);

            if (fornecedor == null) return NotFound();
            return Ok(fornecedor);
        }

        [HttpPost]
        public async Task<IActionResult> PostFornecedor([FromBody] Fornecedor fornecedor)
        {
            await _service.InsertAsync(fornecedor);
            return CreatedAtAction("GetFornecedor", new { id = fornecedor.Id }, fornecedor);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFornecedor([FromRoute] int id)
        {
            var fornecedor = await _service.DeleteAsync(id);
            return Ok(fornecedor);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutFornecedor([FromRoute] int id, [FromBody] Fornecedor fornecedor)
        {
            if (id != fornecedor.Id) return BadRequest();

            await _service.UpdateAsync(fornecedor);
            return NoContent();
        }
    }
}